FROM ruby:2.4.1-alpine

RUN apk add --update \
    bash \
    build-base \
    libxml2-dev \
    libxslt-dev \
    ca-certificates \
    postgresql-dev \
    && rm -rf /var/cache/apk/*

WORKDIR /app
COPY Gemfile Gemfile.lock /app/
RUN bundle install --without test development

COPY src /app

CMD bundle exec rackup -o 0.0.0.0 -p 9292
