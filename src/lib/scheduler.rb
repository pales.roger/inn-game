require 'rufus-scheduler'

Rufus::Scheduler.s.every '3s', allow_overlapping: false do
  begin
    ActiveRecord::Base.connection_pool.with_connection do
      Factory.all.each(&:collect_resources)
    end
  ensure
    ActiveRecord::Base.connection_pool.release_connection
  end
end

Rufus::Scheduler.s.every '1h', allow_overlapping: false do
  if ActiveRecord::Base.connection_pool.connections.count > 10
    ActiveRecord::Base.connection_pool.disconnect!
  end
end
