require_relative '../models/user'
module ApiHelpers

  AUTH = 'X-User-Token'

  module_function

  attr_reader :current_user

  def authenticate!
    @current_user = User.find_by(authentication_token: headers[AUTH])
    return if @current_user
    error!({ error: 'Unauthorized' }, 401)
  end

  def not_found
    error!({ error: 'not found' }, 404)
  end
end
