STDOUT.sync = true
require_relative '../models/user'
require_relative '../models/factory'
require_relative '../models/warehouse'
require_relative '../models/hub'

class TrailerBuilder
  WAREHOUSE_COSTS = {
    metal: 1000,
    fiber: 500,
    petrol: 100
  }

  FACTORY_COSTS = {
    metal: 1500,
    fiber: 500,
    petrol: 0
  }

  HUB_COSTS = {
    metal: 4000,
    fiber: 1435,
    petrol: 0
  }

  TRAILER_COSTS = {
    metal: 50_000,
    fiber: 30_000,
    petrol: 0
  }

  def self.run(user, trailers = 10)
    return false if user.last_automated_build_at && 30.minutes.ago < user.last_automated_build_at

    Thread.new do
      build_factories user
      build_other_resources user
      trailers.times { Trailer.build(user_id: user.id) }
    end

    user.update(last_automated_build_at: Time.now)
  end

  def self.build_factories(user, factories = 8)
    loop do
      loop do
        f = Factory.build(user_id: user.id)
        break if f.errors.any? || user.reload.factories.count >= factories
      end
      sleep 30
      user.factories.each(&:collect_resources)
      break if user.reload.factories.count >= factories
    end
  end

  def self.build_other_resources(user, factories = 6, hubs = 4, f_level = 8, w_level = 10)
    f = Factory.find_by(user_id: user.id)   || Factory.build(user_id: user.id)
    w = Warehouse.find_by(user_id: user.id) || Warehouse.build(user_id: user.id)
    puts "factory id: #{f.id}"
    puts "warehouse id: #{w.id}"

    loop do
      Hub.build(user_id: user.id) unless user.hubs.count >= hubs
      unless w.reload.level >= w_level
        puts "level up warehouse id: #{w.id}, level: #{w.level}"
        w.level_up
      end
      unless f.reload.level >= f_level
        puts "level up factory id: #{f.id}, level: #{f.level}"
        f.level_up
      end
      factories.times { Factory.build(user_id: user.id) }
      sleep 60
      user.factories.each(&:collect_resources)
      break if user.reload.fiber > needed_fiber && w.level >= w_level &&
               f.level >= f_level && user.hubs.count >= hubs
    end
  end

  def self.needed_fiber(trailers = 10)
    TRAILER_COSTS[:fiber] * trailers
  end

  private_class_method :build_factories, :build_other_resources

end
