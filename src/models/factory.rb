require 'active_record'
require_relative 'user'
require_relative 'concerns/buildable'
require_relative 'concerns/sellable'

class Factory < ActiveRecord::Base
  belongs_to  :user
  before_save :set_colleted_at, if: :new_record?

  COST = {
    metal: 1500,
    fiber:  500
  }

  COST_INCREMENT = 0.15

  BUILD_TIME = 30

  PRODUCE_RESOURCES = {
    metal: 2000,
    fiber: 1000,
    petrol: 20
  }

  MAX_LEVEL = nil

  include Buildable
  include Sellable

  def self.build(params = {})
    o = new(params)
    o.build
    o
  end

  def collect_resources
    return false unless collected_at < 1.minute.ago
    $mutex.synchronize do
      u = self.user.reload
      PRODUCE_RESOURCES.each_pair do |resource, value|
        u.send("#{resource}=", u.send(resource) + value)
      end
      u.save
      update(collected_at: Time.now)
    end
  end

  private_class_method :create

  private

  def set_colleted_at
    self.collected_at = Time.now + BUILD_TIME
  end

end
