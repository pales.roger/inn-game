require 'active_record'
require_relative 'user'
require_relative 'concerns/buildable'
require_relative 'concerns/sellable'

class Warehouse < ActiveRecord::Base
  belongs_to :user

  COST = {
    metal: 1000,
    fiber:  500,
    petrol: 100
  }

  COST_INCREMENT = 0.2

  BUILD_TIME = 30

  MAX_LEVEL = nil

  include Buildable
  include Sellable

  def self.build(params = {})
    o = new(params)
    o.build
    o
  end
end
