require 'active_record'
require_relative 'user'
require_relative 'concerns/buildable'
require_relative 'concerns/sellable'

class Hub < ActiveRecord::Base
  belongs_to :user

  COST = {
    metal: 4000,
    fiber: 1435
  }

  COST_INCREMENT = 0.2

  BUILD_TIME = 30

  MAX_LEVEL = 3

  include Buildable
  include Sellable

  def self.build(params = {})
    o = new(params)
    o.build
    o
  end

  private_class_method :create
end
