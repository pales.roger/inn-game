STDOUT.sync = true

module Buildable

  def level_up
    return false if self.build_in_progress || !meet_requirements?
    build
  end

  def meet_requirements?
    check_requirements
    self.errors.empty?
  end

  def build
    return unless meet_requirements?
    $mutex.synchronize do
      u = self.user.reload
      multiplier = 1 + self.level * self.class::COST_INCREMENT
      self.class::COST.each_pair do |resource, value|
        resource_cost = value * multiplier
        u.send("#{resource}=", (u.send(resource) - resource_cost).to_i)
      end
      u.save
    end
    build_procedure
  end

  def build_procedure
    self.build_in_progress = true
    save
    Thread.new do
      sleep self.class::BUILD_TIME
      $mutex.synchronize do
        self.reload
        self.level += 1
        self.build_in_progress = false
        self.capacity += 2 if self.respond_to?(:capacity) && self.level > 1
        save
      end
    end
  end

  def check_requirements
    unless self.class::MAX_LEVEL.nil?
      errors[:level] << 'Reached maximum level' unless level < self.class::MAX_LEVEL
    end

    multiplier = 1 + (level - 1) * self.class::COST_INCREMENT

    self.class::COST.each_pair do |resource, value|
      resource_cost = value * multiplier
      self.errors[resource] << "Not enough #{resource}" if self.user.reload.send(resource) < resource_cost
    end

    self.errors[:build_in_progress] << "Build in progress" if self.build_in_progress
  end
end
