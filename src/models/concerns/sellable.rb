module Sellable

  def sell(metal:, fiber:, petrol:)
    if self.build_in_progress
      self.errors[:build_in_progress] << 'cannot sell while building'
      return false
    end
    update(for_sale: true, sale_metal: metal.to_i, sale_fiber: fiber.to_i, sale_petrol: petrol.to_i)
  end

  def buy(buyer:)
    $mutex.synchronize do
      return false unless self.for_sale
      return false unless meet_sell_requirements?(buyer)
      buyer.metal  -= self.sale_metal
      buyer.fiber  -= self.sale_fiber
      buyer.petrol -= self.sale_petrol
      buyer.save

      u = self.user
      u.metal   += self.sale_metal
      u.fiber   += self.sale_fiber
      u.petrol  += self.sale_petrol
      u.save
      update(sale_fiber: 0, sale_metal: 0, sale_petrol: 0, user_id: buyer.id, for_sale: false)
    end
  end

  def meet_sell_requirements?(buyer)
    check_sell_requirements(buyer)
    self.errors.empty?
  end

  def check_sell_requirements(buyer)
    self.errors[:metal] << 'not enough metal' if buyer.metal < self.sale_metal
    self.errors[:fiber] << 'not enough fiber' if buyer.fiber < self.sale_fiber
    self.errors[:petrol] << 'not enough petrol' if buyer.petrol < self.sale_petrol
  end

end
