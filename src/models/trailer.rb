require 'active_record'
require_relative 'user'
require_relative 'concerns/sellable'

class Trailer < ActiveRecord::Base
  belongs_to :user

  COST = {
    metal: 50_000,
    fiber: 30_000
  }

  BUILD_TIME = 60

  include Sellable

  private_class_method :create

  def self.build(params = {})
    o = new(params)
    o.build
    o
  end

  def level_up!
    raise 'Invalid' unless meet_requirements?
    build
  end

  def meet_requirements?
    check_requirements
    self.errors.empty?
  end

  def build
    if meet_requirements?
      u = self.user
      COST.each_pair do |resource, cost|
        u.send("#{resource}=", u.send(resource) - cost)
      end
      u.save
      self.build_in_progress = true
      save
      Thread.new do
        sleep BUILD_TIME
        self.build_in_progress = false
        save
      end
      true
    else
      false
    end
  end

  private

  def check_requirements
    errors[:warehouses] << 'Level not enough' if user.warehouses.maximum('level').to_i < 10
    errors[:factories]  << 'Level not enough' if user.factories.maximum('level').to_i < 8
    errors[:hubs]       << 'No parking space' unless user.has_parking_space?

    COST.each_pair do |resource, cost|
      errors[resource] << "Not enough #{resource}" if user.send(resource) < cost
    end

    errors[:build_in_progress] << "Build in progress" if build_in_progress
  end

end
