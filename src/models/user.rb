require 'active_record'
require 'digest/sha1'
require 'securerandom'
require_relative 'hub'
require_relative 'factory'
require_relative 'trailer'
require_relative 'warehouse'

class User < ActiveRecord::Base
  has_many :hubs
  has_many :trailers
  has_many :warehouses
  has_many :factories

  validates :email,  uniqueness: true
  validates :metal,  numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :fiber,  numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :petrol, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def self.build(params = {})
    raise 'email and/or password not provided' unless params[:password] && params[:email]
    params.tap do |h|
      h[:encrypted_password] = Digest::SHA1.hexdigest(h[:password])
      h[:authentication_token] = SecureRandom.uuid unless h[:authentication_token]
      h.delete(:password)
    end
    User.new params
  end

  def self.create_session(params)
    encrypted_password = Digest::SHA1.hexdigest(params[:password])
    user = User.find_by(email: params[:email], encrypted_password: encrypted_password)
    raise 'invalid email and/or password' unless user
    user.authentication_token = SecureRandom.uuid
    user.save!
    user
  end

  def update_password(new_password)
    update_attributes!(encrypted_password: Digest::SHA1.hexdigest(new_password))
  end

  def has_parking_space?
    slots = hubs.map(&:capacity).sum
    slots -= trailers.count
    slots > 0
  end

  def auth_token_refresh
    update_attributes(authentication_token: SecureRandom.uuid)
  end

  private_class_method :create
end
