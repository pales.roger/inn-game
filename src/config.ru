require 'grape'
require 'rack/cors'

require_relative 'db/postgres'
Postgres.connect

use Rack::Config do |env|
  env['api.tilt.root'] = 'views/'
end

use Rack::Cors do
  allow do
    origins '*'
    resource '*', hegaders: :any, methods: :any
  end
end

$mutex = Mutex.new

require_relative 'lib/scheduler'

require_relative 'api/base_api'
run BaseApi.new
