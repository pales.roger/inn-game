require 'yaml'
require 'active_record'

class Postgres

  def self.connect
    ActiveRecord::Base.default_timezone = :utc
    ActiveRecord::Base.establish_connection(self.connection_properties)
  end

  def self.connection_properties
    prod_env = {
      'host' => ENV['POSTGRES_HOST'] || 'localhost',
      'database' => ENV['POSTGRES_DATABASE'],
      'username' => ENV['POSTGRES_USER'],
      'password' => ENV['POSTGRES_PASSWORD'],
      'port' => 5432
    }

    environment = ENV['RACK_ENV'] || 'development'
    db_base = YAML::load(File.open(File.join(File.dirname(__FILE__), '../config/database.yml')))

    return db_base[environment] unless environment == 'production'
    db_base[environment].merge!(prod_env)
  end

end
