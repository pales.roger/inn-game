class CreateHubs < ActiveRecord::Migration[5.0]
  def change
    create_table :hubs do |t|
      t.integer   :user_id
      t.integer   :level, default: 0
      t.integer   :capacity, default: 3
      t.boolean   :build_in_progress, defaul: true
      t.boolean   :for_sale, default: false
      t.integer   :sale_metal, default: 0
      t.integer   :sale_fiber, default: 0
      t.integer   :sale_petrol, default: 0
    end
  end
end
