class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string  :email
      t.string  :encrypted_password
      t.string  :authentication_token

      # resources
      t.integer :metal,  default: 1500
      t.integer :fiber,  default: 600
      t.integer :petrol, default: 0

      t.datetime :last_automated_build_at
    end
  end
end
