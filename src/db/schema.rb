# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 5) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "factories", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "level", default: 0
    t.datetime "collected_at"
    t.boolean "build_in_progress"
    t.boolean "for_sale", default: false
    t.integer "sale_metal", default: 0
    t.integer "sale_fiber", default: 0
    t.integer "sale_petrol", default: 0
  end

  create_table "hubs", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "level", default: 0
    t.integer "capacity", default: 3
    t.boolean "build_in_progress"
    t.boolean "for_sale", default: false
    t.integer "sale_metal", default: 0
    t.integer "sale_fiber", default: 0
    t.integer "sale_petrol", default: 0
  end

  create_table "trailers", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.boolean "build_in_progress"
    t.boolean "for_sale", default: false
    t.integer "sale_metal", default: 0
    t.integer "sale_fiber", default: 0
    t.integer "sale_petrol", default: 0
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email"
    t.string "encrypted_password"
    t.string "authentication_token"
    t.integer "metal", default: 1500
    t.integer "fiber", default: 600
    t.integer "petrol", default: 0
    t.datetime "last_automated_build_at"
  end

  create_table "warehouses", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "level", default: 0
    t.boolean "build_in_progress"
    t.boolean "for_sale", default: false
    t.integer "sale_metal", default: 0
    t.integer "sale_fiber", default: 0
    t.integer "sale_petrol", default: 0
  end

end
