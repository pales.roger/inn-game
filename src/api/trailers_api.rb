require 'grape'
require_relative '../lib/api_helpers'
require_relative '../models/user'
require_relative '../models/trailer'
require_relative '../services/trailer_builder'

class TrailersApi < Grape::API

  helpers ApiHelpers

  format :json

  before do
    authenticate!
  end

  namespace :trailers do
    post do
      trailer = Trailer.build(user_id: @current_user.id)
      if trailer.errors.any?
        error!({ errors: trailer.errors.messages }, 400)
      else
        status 201
        trailer.as_json
      end
    end

    params do
      requires :trailer_id, type: Integer
    end
    delete ':trailer_id' do
      trailer = Trailer.find_by(user_id: @current_user.id, id: params[:trailer_id])
      return not_found unless trailer

      trailer.destroy.as_json
    end

    params do
      requires :count, type: Integer
    end
    post 'automated_build' do
      success = TrailerBuilder.run(@current_user, params[:count])
      next_run_at = @current_user.last_automated_build_at + 30.minutes
      return error!({error: "service available at #{next_run_at}"}, 400) unless success
      { status: 'enqueued' }
    end

    params do
      requires :trailer_id, type: Integer
    end
    put ':trailer_id/buy' do
      trailer = Trailer.find_by(for_sale: true, id: params[:trailer_id])

      return not_found unless trailer

      return { status: 'sold' } if trailer.buy(buyer: @current_user)

      error!({ errors: trailer.errors.messages }, 400)
    end

    params do
      requires :trailer_id, type: Integer
      optional :metal, type: Integer
      optional :petrol, type: Integer
      optional :fiber, type: Integer
      optional :cancel_sell, type: Boolean
    end
    put ':trailer_id/sell' do
      trailer = Trailer.find_by(user_id: @current_user.id, id: params[:trailer_id])

      return not_found unless trailer

      if params[:cancel_sell]
        trailer.update(for_sale: false)
      else
        trailer.sell(metal: params[:metal], petrol: params[:petrol], fiber: params[:fiber])
      end

      return error!({ errors: trailer.errors.messages }, 400) unless trailer.errors.empty?

      { status: 'ok' }
    end
  end
end
