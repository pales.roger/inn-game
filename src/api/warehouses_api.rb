require 'grape'
require_relative '../lib/api_helpers'
require_relative '../models/user'
require_relative '../models/warehouse'

class WarehousesApi < Grape::API

  helpers ApiHelpers

  format :json

  before do
    authenticate!
  end

  namespace :warehouses do
    post do
      warehouse = Warehouse.build(user_id: @current_user.id)
      if warehouse.errors.any?
        error!({ errors: warehouse.errors.messages }, 400)
      else
        status 201
        warehouse.as_json
      end
    end

    params do
      requires :warehouse_id, type: Integer
    end
    put ':warehouse_id/level_up' do
      warehouse = Warehouse.where(user_id: @current_user.id, id: params[:warehouse_id]).first
      return not_found unless warehouse

      warehouse.level_up

      return warehouse.as_json if warehouse.errors.empty?

      error!({ errors: warehouse.errors.messages }, 400)
    end

    params do
      requires :warehouse_id, type: Integer
    end
    put ':warehouse_id/buy' do
      warehouse = Warehouse.find_by(for_sale: true, id: params[:warehouse_id])

      return not_found unless warehouse

      return { status: 'sold' } if warehouse.buy(buyer: @current_user)

      error!({ errors: warehouse.errors.messages }, 400)
    end

    params do
      requires :warehouse_id, type: Integer
      optional :metal, type: Integer
      optional :petrol, type: Integer
      optional :fiber, type: Integer
      optional :cancel_sell, type: Boolean
    end
    put ':warehouse_id/sell' do
      warehouse = Warehouse.find_by(user_id: @current_user.id, id: params[:warehouse_id])

      return not_found unless warehouse

      if params[:cancel_sell]
        warehouse.update(for_sale: false)
      else
        warehouse.sell(metal: params[:metal], petrol: params[:petrol], fiber: params[:fiber])
      end

      return error!({ errors: warehouse.errors.messages }, 400) unless warehouse.errors.empty?

      { status: 'ok' }
    end

    params do
      requires :warehouse_id, type: Integer
    end
    delete ':warehouse_id' do
      warehouse = Warehouse.where(user_id: @current_user.id, id: params[:warehouse_id]).first
      return not_found unless warehouse

      warehouse.destroy.as_json
    end
  end
end
