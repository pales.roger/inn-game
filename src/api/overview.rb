# frozen_string_literal: true
require 'uri'
require 'grape'
require 'grape-erb'
require 'erb'
require_relative '../lib/api_helpers'
require_relative '../models/user'
require_relative '../models/hub'
require_relative '../models/factory'
require_relative '../models/trailer'
require_relative '../models/warehouse'

class Overview < Grape::API
  helpers ApiHelpers

  content_type :json, 'application/json'
  content_type :html, 'text/html'
  formatter :html, Grape::Formatter::Erb
  format :html

  namespace :overview do

    get 'login', erb: 'login'

    get '/', erb: 'index' do
      @user = User.find_by(authentication_token: cookies['userAuthToken'])
      redirect '/overview/login' if @user.nil?
    end

    get '/marketplace', erb: 'marketplace' do
      @user = User.find_by(authentication_token: cookies['userAuthToken'])
      if @user.nil?
        redirect '/overview/login'
      else
        @trailers = Trailer.where(for_sale: true).where.not(user_id: @user.id)
        @factories = Factory.where(for_sale: true).where.not(user_id: @user.id)
        @hubs = Hub.where(for_sale: true).where.not(user_id: @user.id)
        @warehouses = Warehouse.where(for_sale: true).where.not(user_id: @user.id)
      end
    end

    get 'logout', erb: 'login' do
      @user.auth_token_refresh if @user
    end
  end
end
