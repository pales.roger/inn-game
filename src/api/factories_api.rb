require 'grape'
require_relative '../lib/api_helpers'
require_relative '../models/user'
require_relative '../models/factory'

class FactoriesApi < Grape::API

  helpers ApiHelpers

  format :json

  before do
    authenticate!
  end

  namespace :factories do
    post do
      factory = Factory.build(user_id: @current_user.id)
      if factory.errors.any?
        error!({ errors: factory.errors.messages }, 400)
      else
        status 201
        factory.as_json
      end
    end

    params do
      requires :factory_id, type: Integer
    end
    put ':factory_id/buy' do
      factory = Factory.find_by(for_sale: true, id: params[:factory_id])

      return not_found unless factory

      return { status: 'sold' } if factory.buy(buyer: @current_user)

      error!({ errors: factory.errors.messages }, 400)
    end

    params do
      requires :factory_id, type: Integer
      optional :metal, type: Integer
      optional :petrol, type: Integer
      optional :fiber, type: Integer
      optional :cancel_sell, type: Boolean
    end
    put ':factory_id/sell' do
      factory = Factory.find_by(user_id: @current_user.id, id: params[:factory_id])

      return not_found unless factory

      if params[:cancel_sell]
        factory.update(for_sale: false)
      else
        factory.sell(metal: params[:metal], petrol: params[:petrol], fiber: params[:fiber])
      end

      return error!({ errors: factory.errors.messages }, 400) unless factory.errors.empty?

      { status: 'ok' }
    end

    params do
      requires :factory_id, type: Integer
    end
    put ':factory_id/level_up' do
      factory = Factory.where(user_id: @current_user.id, id: params[:factory_id]).first
      return not_found unless factory

      factory.level_up

      return factory.as_json if factory.errors.empty?

      error!({ errors: factory.errors.messages }, 400)
    end

    params do
      requires :factory_id, type: Integer
    end
    delete ':factory_id' do
      factory = Factory.find_by(user_id: @current_user.id, id: params[:factory_id])
      return not_found unless factory

      factory.destroy.as_json
    end
  end
end
