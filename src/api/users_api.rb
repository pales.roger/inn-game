require 'grape'
require_relative '../lib/api_helpers'
require_relative '../models/user'

class UsersApi < Grape::API

  helpers ApiHelpers

  format :json

  params do
    requires :email, type: String, allow_blank: false, regexp: /.+@.+/
    requires :password, type: String, allow_blank: false
  end
  post :register do
    user = User.build(params)
    user.save!

    status 201
    user.as_json(except: [:encrypted_password])
  end

  params do
    requires :email, type: String, allow_blank: false, regexp: /.+@.+/
    requires :password, type: String, allow_blank: false
  end
  post :session do
    status 201
    User.create_session(params).as_json(except: [:encrypted_password])
  end

  before do
    authenticate!
  end

  get :current_user do
    @current_user.as_json(except: [:encrypted_password], include: [:trailers, :factories, :hubs, :warehouses])
  end

  params do
    requires :password, type: String, allow_blank: false
  end
  put :password do
    @current_user.update_password(params[:password])
    { message: 'password updated' }
  end
end
