require 'grape'
require_relative '../lib/api_helpers'
require_relative '../models/user'
require_relative '../models/hub'

class HubsApi < Grape::API

  helpers ApiHelpers

  format :json

  before do
    authenticate!
  end

  namespace :hubs do
    post do
      hub = Hub.build(user_id: @current_user.id)
      if hub.errors.any?
        error!({ errors: hub.errors.messages }, 400)
      else
        status 201
        hub.as_json
      end
    end

    params do
      requires :hub_id, type: Integer
    end
    put ':hub_id/level_up' do
      hub = Hub.where(user_id: @current_user.id, id: params[:hub_id]).first
      return not_found unless hub

      hub.level_up

      return hub.as_json if hub.errors.empty?

      error!({ errors: hub.errors.messages }, 400)
    end

    params do
      requires :hub_id, type: Integer
    end
    put ':hub_id/buy' do
      hub = Hub.find_by(for_sale: true, id: params[:hub_id])

      return not_found unless hub

      return { status: 'sold' } if hub.buy(buyer: @current_user)

      error!({ errors: hub.errors.messages }, 400)
    end

    params do
      requires :hub_id, type: Integer
      optional :metal, type: Integer
      optional :petrol, type: Integer
      optional :fiber, type: Integer
      optional :cancel_sell, type: Boolean
    end
    put ':hub_id/sell' do
      hub = Hub.find_by(user_id: @current_user.id, id: params[:hub_id])

      return not_found unless hub

      if params[:cancel_sell]
        hub.update(for_sale: false)
      else
        hub.sell(metal: params[:metal], petrol: params[:petrol], fiber: params[:fiber])
      end

      return error!({ errors: hub.errors.messages }, 400) unless hub.errors.empty?

      { status: 'ok' }
    end

    params do
      requires :hub_id, type: Integer
    end
    delete ':hub_id' do
      hub = Hub.where(user_id: @current_user.id, id: params[:hub_id]).first
      return not_found unless hub

      hub.destroy.as_json
    end
  end
end
