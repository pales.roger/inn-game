require 'grape'
require 'grape-erb'
require 'tilt'
require_relative '../lib/api_helpers'
require_relative 'warehouses_api'
require_relative 'trailers_api'
require_relative 'hubs_api'
require_relative 'factories_api'
require_relative 'users_api'
require_relative 'overview'

class BaseApi < Grape::API

  helpers ApiHelpers

  rescue_from :all do |e|
    puts e.message
    puts e.backtrace
    error!({ class: e.class.name, error: e.message, backtrace: e.backtrace }, 500)
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    not_found
  end

  get '/' do
    @user = User.find_by(authentication_token: cookies['userAuthToken'])
    if @user
      redirect '/overview'
    else
      redirect '/overview/login'
    end
  end

  get 'status' do
    { status: 'alive' }
  end

  namespace :assets do
    get ':file.:ext' do
      filename = File.join File.dirname(__FILE__), "../assets/#{params[:file]}.#{params[:ext]}"
      if File.file? filename
        content_type "text/#{params[:ext]}"
        File.read(filename)
      else
        error!('Not found', 404)
      end
    end
  end

  mount UsersApi
  mount FactoriesApi
  mount HubsApi
  mount TrailersApi
  mount WarehousesApi
  mount Overview
end
