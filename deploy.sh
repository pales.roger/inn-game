#!/bin/bash

docker run -d --name inn_postgres \
-e POSTGRES_DATABASE=inn_game \
-e POSTGRES_USER=inn_game \
-e POSTGRES_PASSWORD=inn_game \
-v /mnt/inn_postgres_data:/var/lib/postgresql/data \
-p 5432:5432 \
postgres

docker run --rm \
-e POSTGRES_DATABASE=inn_game \
-e POSTGRES_USER=inn_game \
-e POSTGRES_PASSWORD=inn_game \
-e RACK_ENV=production \
-e POSTGRES_HOST=inn_postgres \
--link inn_postgres:inn_postgres \
-p 80:9292 \
rogerpales/inn_game:1 \
bundle exec rake db:drop db:create db:migrate

docker run -d --name inn_game \
-e POSTGRES_DATABASE=inn_game \
-e POSTGRES_USER=inn_game \
-e POSTGRES_PASSWORD=inn_game \
-e RACK_ENV=production \
-e POSTGRES_HOST=inn_postgres \
--link inn_postgres:inn_postgres \
-p 80:9292 \
rogerpales/inn_game:1
